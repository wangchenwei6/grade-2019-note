## 本节目标

1. 了解wordpress
2. 熟悉wordpress的安装

## 博客系统

博客是非常经典的内容管理系统之一，从博客系统可以演化出其他的系统，比如问答、贴吧、论坛、图片、音乐、视频等系统。

这些系统其实大同小异，本质都是内容管理。

这边参考wordpress，实现wordpress的功能，wordpress官方地址：

	https://cn.wordpress.org/news/
	
下面我们会下载尝试下载wordpress并安装，最后查看安装后的页面。

## wordpress介绍

WordPress是使用PHP语言开发的博客平台，用户可以在支持PHP和MySQL数据库的服务器上架设属于自己的网站。也可以把 WordPress当作一个内容管理系统（CMS）来使用。

WordPress是一款个人博客系统，并逐步演化成一款内容管理系统软件，它是使用PHP语言和MySQL数据库开发的,用户可以在支持 PHP 和 MySQL数据库的服务器上使用自己的博客。

WordPress有许多第三方开发的免费模板，安装方式简单易用。不过要做一个自己的模板，则需要你有一定的专业知识。比如你至少要懂的标准通用标记语言下的一个应用HTML代码、CSS、PHP等相关知识。

WordPress官方支持中文版，同时有爱好者开发的第三方中文语言包，如wopus中文语言包。WordPress拥有成千上万个各式插件和不计其数的主题模板样式。

## wordpress运行环境

wordpress采用php+mysql进行开发的，所以需要确保php和mysql环境

## 下载wordpress

前往官方地址下载即可：

	https://cn.wordpress.org/download/

下载完成后，这边我们下载的是5.6.2版本，是一个压缩文件，尝试查看压缩文件的内容，可以看到是一堆的代码文件。

## 安装wordpress

### 1. 解压wordpress

将wordpress源码解压到web根目录下：

	D:\nginx-1.18.0\html\

解压后的完成路径应该是：

	D:\nginx-1.18.0\html\wordpress

### 2. 配置nginx

nginx设置一个新的端口进行访问项目，假定设置为8889，nginx配置参考如下：

	server {
		listen       8889;
		server_name  localhost;
		root   D:/nginx-1.18.0/html/wordpress;

		location / {
				index  index.html index.htm index.php;
		}

		error_page   500 502 503 504  /50x.html;
		location = /50x.html {
				root   html;
		}

		location ~ \.php$ {
				fastcgi_pass   127.0.0.1:9001;
				fastcgi_index  index.php;
				fastcgi_param  SCRIPT_FILENAME  $document_root$fastcgi_script_name;
				include        fastcgi_params;
		}
	}

配置完成后，重启nginx服务器。

### 3. 开启mysql服务

打开services，找到MySQL57服务，开启即可。

备注：安装mysql，可以

### 4. 访问wordpress进行安装

访问 http://localhost:8889/ ，根据提示，一步步安装就可以了。

安装成功后查看页面。

![](02_博客系统_files/1.jpg)

后台管理页面：

![](02_博客系统_files/2.jpg)

## 课堂练习

1. 下载wordpress并安装，安装成功后进行截图。

