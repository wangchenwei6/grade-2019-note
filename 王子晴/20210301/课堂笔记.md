1.20210301的作业内容:一是简答题，对git的了解；二是基础操作题，初始化仓库git init、增加add commit和修改文件。
2.20210301的课堂知识点: git --version（版本信息）
                       git init（初始化）或 git init 文件夹名
                       git status（版本库里的文件变动情况，如增删改） 或 git status -s（简洁版）
                       git add 文件名或文件路径（加入到暂存区）
                       git commit -m ""（将暂存区的东西提交到版本库，提交变更记录）
                       git log (提交信息，历史记录)
                       git diff（比较变更信息）                       