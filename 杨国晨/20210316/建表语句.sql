create table classify(
	classify_id int(11) unsigned auto_increment comment '分类id', 
	classify_name varchar(20) not null comment '分类名称',
	add_time int(11) not null default 0 comment '增加时间',
	update_time int(11) not null comment '修改时间',
	primary key(classify_id)
)engine=innodb default charset=utf8 comment '分类表'

alter table classify add classify_describe varchar(1000) not null comment '分类描述' after classify_name

create table article(
 article_id int(11) unsigned auto_increment comment '文章id',
 article_title varchar(20) not null comment '文章标题',
 article_intro varchar(30) not null comment '文章简介',
 article_content varchar(10000) not null comment '文章内容',
 add_time int(11) not null default 0 comment '增加时间',
 update_time int(11) not null comment '修改时间',
 primary key (article_id)
)engine=innodb default charset=utf8 comment '文章表'