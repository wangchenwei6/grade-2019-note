alter table employee_info add sex tinyint(3) default 3 comment '员工性别：1男2女3保密'
alter table employee_info add address varchar(100) not null default ' ' comment '住址'
alter table employee_info add join_time int(11) not null comment '入职时间'
alter table employee_info change salary salary decimal(11,2) not null default 0 comment '工资'