<?php

$dsn = "sqlsrv:Server=localhost;Database=Task";
$db = new PDO($dsn, "sa", "123456");

$sql = "SELECT  top 50 * FROM Task ";
$result = $db->query($sql); // 查询sql
$taskList = $result->fetchAll(PDO::FETCH_ASSOC);

?><!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>列表</title>
    <link rel="stylesheet" type="text/css" href="css/main.css" />
    <script src="js/jquery.js"></script>
</head>
<body>
<div id="container">
    <div id="login_info">
        欢迎你：admin
        <a href="logout.html">退出登录</a>
    </div>
    <div id="choose_div">
        <button id="all">全选</button>
        <a id="multi_delete" href="javascript:void(0);">删除选中任务</a>

        <a id="add" href="task_add.php" style="float: right">增加任务</a>
    </div>
    <form id="list_from" action="task_delete_multi.php" method="post">
    <table class="list">
        <tbody>
        <tr>
            <th></th>
            <th>任务id</th>
            <th>任务名称</th>
            <th>任务状态</th>
            <th>创建时间</th>
            <th>修改时间</th>
            <th>操作</th>
        </tr>
        <?php foreach ($taskList as $item): ?>
        <tr>
            <td><input type="checkbox" class="task-checkbox" name="TaskId[]" value="<?php echo $item['TaskId'];?>"></td>
            <td><?php echo $item['TaskId'];?></td>
            <td><?php echo $item['TaskName'];?></td>
            <td><?php
                if ( $item['TaskStatus'] ==1){
                    echo "新创建";
                }else if ($item['TaskStatus'] ==2){
                    echo "进行中";
                }else if ($item['TaskStatus'] ==3){
                    echo "已完成";
                }else{
                    echo "未知状态";
                }
                ?></td>
            <td><?php echo $item['TaskCreateTime'];?></td>
            <td><?php echo $item['TaskUpdateTime'];?></td>
            <td>
                <a class="update_ing" href="task_detail.php?id=<?php echo $item['TaskId']; ?>">详情</a>
                <?php if ($item['TaskStatus'] == 1): ?>
                    <a class="delete" href="task_delete.php?id=<?php echo $item['TaskId']; ?>">删除</a>
                <?php endif; ?>
            </td>
        </tr>
        <?php endforeach; ?>
        </tbody>
    </table>
    </form>
</div>

<script src="js/main.js"></script>
</body>
</html>
