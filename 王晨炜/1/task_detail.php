<?php
$taskId = $_GET['id'];
if (empty($taskId)){
    echo "任务id参数错误";
    exit();
}

$dsn = "sqlsrv:Server=localhost;Database=Task";
$db = new PDO($dsn, "sa", "123456");

$sql = "SELECT  * FROM Task where TaskId = '".$taskId."'";
$result = $db->query($sql); // 查询sql
$taskInfo = $result->fetch(PDO::FETCH_ASSOC);


?><!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>任务详情</title>
    <link rel="stylesheet" type="text/css" href="css/main.css"/>
    <script src="js/jquery.js"></script>
</head>
<body>
<div id="container">
    <a href="task_list.php">返回任务列表</a>
    <form>
        <table class="update">
            <caption>
                <h3>任务详情</h3>
            </caption>
            <tr>
                <td>任务名称：</td>
                <td><?php echo $taskInfo['TaskName']; ?></td>
            </tr>
            <tr>
                <td>任务状态：</td>
                <td><?php
                    if ( $taskInfo['TaskStatus'] ==1){
                        echo "新创建";
                    }else if ($taskInfo['TaskStatus'] ==2){
                        echo "进行中";
                    }else if ($taskInfo['TaskStatus'] ==3){
                        echo "已完成";
                    }else{
                        echo "未知状态";
                    }
                    ?></td>
            </tr>
            <tr>
                <td>任务内容：</td>
                <td><textarea  name="TaskContent" cols="60" rows="15"
                              readonly="readonly"><?php echo $taskInfo['TaskContent']; ?></textarea></td>
            </tr>
            <tr>
                <td></td>
                <td>
                    <?php if ($taskInfo['TaskStatus'] == 1): ?>
                    <a class="update_ing" href="task_update_ing.php?id=<?php echo $taskInfo['TaskId'];?>">标记为进行中</a>
                    <?php endif;?>
                    <?php if ($taskInfo['TaskStatus'] == 2): ?>
                        <a class="update_ing" href="task_update_finish.php?id=<?php echo $taskInfo['TaskId'];?>">标记为已完成</a>
                    <?php endif;?>
                </td>
            </tr>
        </table>
    </form>
</div>
<script src="js/main.js"></script>
</body>
</html>
