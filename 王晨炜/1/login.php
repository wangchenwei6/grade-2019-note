<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>登录</title>
    <link rel="stylesheet" type="text/css" href="css/main.css" />
</head>
<body>
<div id="container">
    <form action="login_check.php" method="post">
        <table class="update login">
            <caption>
                <h3>登录</h3>
            </caption>
            <tr>
                <td>用户名：</td>
                <td><input type="text" name="admin_account"/></td>
            </tr>
            <tr>
                <td>密码：</td>
                <td><input type="password"  name="admin_password"/></td>
            </tr>
            <tr>
                <td></td>
                <td>
                    <input type="submit" value="登录" class="btn" />
                </td>
            </tr>
        </table>
    </form>
</div>
</body>
</html>
