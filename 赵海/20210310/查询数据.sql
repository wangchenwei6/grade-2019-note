-- 1. 查询商品表所有数据。
select * from item; 

-- 2. 查询价格小于2000的商品。
select * from item where item_price < 2000;

-- 3. 查询名称中包含 海信 的商品。
select * from item where item_name like '%海信%';

-- 4. 查询商品表，假设每页3条记录，查询第1页的数据。
select * from item limit 2;

-- 5. 查询商品表，假设每页3条记录，查询第2页的数据。
select * from item limit 2 offset 2;

-- 6. 查询所有商品，按照价格进行升序排序。
select * from item ORDER BY item_price DESC;

-- 7. 查询价格小于2000的商品总数。
select count(*) from item where item_price<2000;

-- 8. 查询购物车中所有数据。
select * from cart;

-- 9. 查询每个用户加入购物车的商品总数。
select user_id,sum(item_num) from cart group by user_id;

-- 10. 查询加入购物车的商品总数小于2的用户。
select user_id from cart group by user_id having sum(item_num)<2;

-- 11. 查询有加入购物车的商品。
select distinct item.* from item inner join cart on item.item_id=cart.item_id;

-- 12. 查询没有加入购物车的商品。
select item.* from item left join cart on item.item_id=cart.item_id where isnull(cart.cart_id);
