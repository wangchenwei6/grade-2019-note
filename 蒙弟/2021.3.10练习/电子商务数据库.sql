-- 创建电子商务数据库eshop
CREATE DATABASE
IF
	NOT EXISTS eshop DEFAULT CHARACTER 
	SET utf8mb4 DEFAULT COLLATE utf8mb4_general_ci;
	
	
-- 	创建商品表item
use eshop;

create table item (
	item_id int(11) unsigned not null AUTO_INCREMENT,
	item_name varchar(100) not null comment '商品名称',
	item_price decimal(10,2) not null comment '商品价格',
	primary key (`item_id`)
) engine = innodb default charset = utf8mb4 comment = '商品表'


-- 创建购物车表cart
create table cart (
	cart_id int(11) unsigned not null auto_increment,
	user_id int(11) unsigned not null comment '用户id',
	item_id int(11) unsigned not null comment '商品id',
	item_num int(11) unsigned not null default '1' comment '商品数量',
	add_time int(11) unsigned not null default '0' comment '加入时间',
	primary key (cart_id)
) engine = innodb default charset = utf8mb4 comment = '购物车表'


-- 商品表：
-- 商品名 	                                                                                              价格  
-- 小米全面屏电视 43英寸 E43K 全高清 DTS-HD 震撼音效 1GB+8GB 智能网络液晶平板电视 L43M5-EK 	              1349
-- TCL 55L8 55英寸 4K超高清电视 智慧语音 超薄机身 杜比+DTS双解码 网络教育 智能液晶平板电视机 	            2199
-- 海信 VIDAA 43V1F-R 43英寸 全高清 海信电视 全面屏电视 智慧屏 1G+8G 教育电视 人工智能液晶平板电视      	1349
-- 海信（Hisense）60E3F 60英寸 4K超高清 智慧语音 超薄悬浮全面屏大屏精致圆角液晶电视机 教育电视 人工智能 	2699
-- TCL 50L8 50英寸 4K超高清电视 智慧语音 超薄机身 杜比+DTS双解码 网络教育 智能液晶平板电视机 	            1999
-- 长虹 55D4P 55英寸超薄无边全面屏 4K超高清 手机投屏 智能网络 教育电视 平板液晶电视机（黑色） 	          2099


insert into item (item_name,item_price) 
	values 
	('小米全面屏电视 43英寸 E43K 全高清 DTS-HD 震撼音效 1GB+8GB 智能网络液晶平板电视 L43M5-EK',1349),
	('TCL 55L8 55英寸 4K超高清电视 智慧语音 超薄机身 杜比+DTS双解码 网络教育 智能液晶平板电视机',2199),
	('海信 VIDAA 43V1F-R 43英寸 全高清 海信电视 全面屏电视 智慧屏 1G+8G 教育电视 人工智能液晶平板电视 ',1349),
	('海信（Hisense）60E3F 60英寸 4K超高清 智慧语音 超薄悬浮全面屏大屏精致圆角液晶电视机 教育电视 人工智能',2699),
	('TCL 50L8 50英寸 4K超高清电视 智慧语音 超薄机身 杜比+DTS双解码 网络教育 智能液晶平板电视机',1999),
	('长虹 55D4P 55英寸超薄无边全面屏 4K超高清 手机投屏 智能网络 教育电视 平板液晶电视机（黑色）',2099);

select * from item



-- 购物车：
-- 用户id 	商品id 	商品数量
-- 1       	1     	1
-- 2 	      2     	2
-- 3      	4 	    1
-- 3      	5 	    1
-- 4 	      1 	    2

insert into cart (user_id,item_id,item_num) values 
	(1,1,1),
	(2,2,2),
	(3,4,1),
	(3,5,1),
	(4,1,2);
	
select * from cart 	
	


	

-- 查询商品表所有数据。
select * from item;

-- 查询价格小于2000的商品。
select * from item where item_price < 2000;

-- 查询名称中包含 海信 的商品。
select * from item where item_name like '%海信%';

-- 查询商品表，假设每页3条记录，查询第1页的数据。
select * from item limit 3;

-- 查询商品表，假设每页3条记录，查询第2页的数据。
select * from item limit 3 offset 3;

-- 查询所有商品，按照价格进行升序排序。
select * from item order by item_price asc; 

-- 查询价格小于2000的商品总数。
select count(*) from item where item_price < 2000;

-- 查询购物车中所有数据。
select * from cart;

-- 查询每个用户加入购物车的商品总数。
select user_id,sum(item_num) from cart group by user_id;

-- 查询加入购物车的商品总数小于2的用户。
select user_id from cart group by user_id having sum(item_num) < 2;


-- 查询有加入购物车的商品。
select distinct item.* from item inner join cart on item.item_id = cart.item_id;

-- 查询没有加入购物车的商品。
select item.* from item left join cart on item.item_id = cart.item_id where isnull(cart.cart_id); 






-- 1.用户4将将商品1从购物车中移除。
delete from cart where user_id = 4 and item_id = 1;
select * from cart;

-- 2.用户3清空购物车。
delete from cart where user_id = 3;

-- 3.移除购物车中的所有数据：
--     使用delete进行删除
delete from cart;

--     使用truncate进行删除
truncate table cart;

--     对比delete和truncate的差别
-- 1.DELETE 删除数据后，自增字段的不会重新计数（从删除的id开始计数）；TRUNCATE 清空表记录后，自增字段的会重新计数（从0开始计数）。
-- 2.DELETE 的使用范围更广，因为它可以通过 WHERE 子句指定条件来删除部分数据；而 TRUNCATE 不支持 WHERE 子句，只能删除整体。

	
	