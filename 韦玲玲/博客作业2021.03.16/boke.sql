/*
Navicat MySQL Data Transfer

Source Server         : admin
Source Server Version : 50730
Source Host           : 127.0.0.1:3306
Source Database       : boke

Target Server Type    : MYSQL
Target Server Version : 50730
File Encoding         : 65001

Date: 2021-03-16 11:39:08
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for artice
-- ----------------------------
DROP TABLE IF EXISTS `artice`;
CREATE TABLE `artice` (
  `artice-id` int(11) NOT NULL,
  `article-title` varchar(100) NOT NULL,
  `category` int(11) NOT NULL,
  `author` varchar(20) NOT NULL,
  `artice-content` varchar(10000) NOT NULL,
  `update-time` int(11) NOT NULL,
  `add-time` int(11) NOT NULL,
  PRIMARY KEY (`artice-id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of artice
-- ----------------------------

-- ----------------------------
-- Table structure for category
-- ----------------------------
DROP TABLE IF EXISTS `category`;
CREATE TABLE `category` (
  `artice-id` int(11) NOT NULL AUTO_INCREMENT,
  `classified_name` varchar(20) NOT NULL,
  `classified_briefing` varchar(1000) NOT NULL,
  `update-time` int(11) NOT NULL,
  `add-time` int(11) NOT NULL,
  PRIMARY KEY (`artice-id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of category
-- ----------------------------
