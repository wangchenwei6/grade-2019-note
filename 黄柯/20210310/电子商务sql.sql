
-- 创建电子商务库
CREATE DATABASE if not EXISTS eshop
	DEFAULT CHARACTER SET utf8mb4
	DEFAULT COLLATE utf8mb4_general_ci;
	
use eshop;
	
-- 	创建商品表
CREATE table item(
	item_id int(11) UNSIGNED not null auto_increment,
	item_name VARCHAR(100) not null COMMENT '商品名称',
	item_price DECIMAL(10,2) not null COMMENT '商品价格',
	PRIMARY KEY(item_id)
)ENGINE=INNODB DEFAULT CHARSET=utf8mb4 COMMENT = '商品表';



-- 创建购物车表
CREATE table cart(
	cart_id int(11) UNSIGNED not null auto_increment,
	user_id int(11) UNSIGNED not null COMMENT '用户id',
	item_id int(11) UNSIGNED not null COMMENT '商品id',
	item_num int(11) UNSIGNED not null DEFAULT '1' COMMENT '商品数量',
	add_time int(11) UNSIGNED not null DEFAULT '0' COMMENT '加入时间',
	PRIMARY KEY(cart_id)
)ENGINE=INNODB DEFAULT CHARSET=utf8mb4 COMMENT = '购物车表';


select * from item;
select * from cart;


INSERT into item(item_name,item_price)
VALUES('长虹全面屏 智能平板电视',1349),
			('4k超高清电视，智慧语音 ',2199),
			('人工智能 海星电视 智能平板电视',1349),
			('全面屏超清电视',2699)


INSERT into cart(user_id,item_id,item_num)
VALUES(1,1,1),
			(2,2,2),
			(3,4,5),
			(3,5,1),
			(4,1,2)





-- --1. 查询商品表所有数据。
 select * from item
 
-- --2. 查询价格小于2000的商品。
 select * from item where item_price < 2000

-- --3. 查询名称中包含 海信 的商品。
 select * from item where item_name like '%海信%'

-- --4. 查询商品表，假设每页3条记录，查询第1页的数据
 select * from item limit 3

-- --5. 查询商品表，假设每页3条记录，查询第2页的数据。
 select * from item limit 3 OFFSET 3

-- --6. 查询所有商品，按照价格进行升序排序。
 select * from item ORDER BY  item_price desc

-- --7. 查询价格小于2000的商品总数。
 select count(item_id) from item where item_price < 2000

-- --8. 查询购物车中所有数据。
 select * from cart

-- --9. 查询每个用户加入购物车的商品总数。
 select user_id, count(item_num) from cart group by user_id

-- --10. 查询加入购物车的商品总数小于2的用户。
 select user_id, item_num from cart group by user_id having item_num<2

-- --11. 查询有加入购物车的商品。
 select item_name from item where item_id in (select item_id from cart);

SELECT
 DISTINCT	item.item_name 
FROM
	item
	INNER JOIN cart on item.item_id = cart.item_id
	
-- --12. 查询没有加入购物车的商品。
 select item_name from item where item_id not in (select item_id from cart)









-- --删除数据

--
-- 1. 用户4将将商品1从购物车中移除。
 delete from cart where user_id = 4 and item_id =1

-- --2. 用户3清空购物车。
 delete from cart where user_id =3

-- --3. 移除购物车中的所有数据：
-- 	1. 使用delete进行删除
-- 	2. 使用truncate进行删除
-- 	3. 对比delete和truncate的差别
 delete from cart
  truncate from cart
	
  delete和truncate的区别？
   --delete不会删除表的自增，索引等
   --truncate会将表的自增，索引重置   
	 














