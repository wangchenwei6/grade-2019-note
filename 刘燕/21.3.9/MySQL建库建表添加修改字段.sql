-- 库
CREATE DATABASE
IF NOT EXISTS employee DEFAULT CHARACTER
SET utf8mb4 DEFAULT COLLATE utf8mb4_general_ci;

-- 信息表
USE employee;

CREATE TABLE employee (
	`id` INT (11) UNSIGNED NOT NULL auto_increment COMMENT '员工编号',
	`name` VARCHAR (25) NOT NULL COMMENT '员工名称',
	`deptId` INT (11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '所在部门编号',
	`salary` FLOAT (11, 2) UNSIGNED NOT NULL DEFAULT 0 COMMENT '工资',
	`update_time` INT (11) UNSIGNED NOT NULL COMMENT '修改时间',
	`add_time` INT (11) UNSIGNED NOT NULL COMMENT '增加时间',
	PRIMARY KEY (`id`)
) ENGINE = INNODB DEFAULT charset = utf8mb4 COMMENT '员工信息表';

-- 部门表
CREATE TABLE department (
	deptId INT (11) UNSIGNED NOT NULL auto_increment COMMENT '部门等级',
	NAME VARCHAR (25) NOT NULL COMMENT '部门名称',
	LEVEL INT (11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '部门等级',
	parentDeptId INT (11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '上级部门编号',
	deptLeader INT (11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '部门领导',
	update_time INT (11) UNSIGNED NOT NULL COMMENT '修改时间',
	add_time INT (11) UNSIGNED NOT NULL COMMENT '增加时间',
	PRIMARY KEY (`deptId`)
) ENGINE = INNODB DEFAULT charset = utf8mb4 COMMENT '部门信息表';

SHOW TABLES;

-- 修改表名字
ALTER TABLE employee RENAME employee_info;

ALTER TABLE department RENAME department_info;

-- 添加字段
ALTER TABLE employee_info ADD sex TINYINT (3) UNSIGNED DEFAULT 3 COMMENT '1男2女3保密' AFTER NAME;

ALTER TABLE employee_info ADD address VARCHAR (100) NOT NULL DEFAULT '' COMMENT '住址' AFTER sex;

ALTER TABLE employee_info ADD join_time INT (11) NOT NULL COMMENT '入职时间' AFTER deptId;

-- 修改字段
ALTER TABLE employee_info CHANGE salary salary DECIMAL (11, 2) UNSIGNED NOT NULL DEFAULT 0 COMMENT '工资';