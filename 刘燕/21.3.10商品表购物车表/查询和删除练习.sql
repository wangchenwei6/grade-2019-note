-- 查询练习
	-- 1.查询商品表所有的数据
select * from item ;
	-- 2.查询价格小于2000的商品
select * from item where item_price<2000;
	-- 3.查询名称中包含海信的产品
select * from item where item_name like'%海信%';
	-- 4。查询商品表，假设每页三条记录，查询第一页的数据
select *from item LIMIT 2;
	-- 5.查询商品表，假设每页三条记录，查询第二页的数据
select *from item LIMIT 2 OFFSET 2;
	-- 6.查询所有商品，按照价格进行升序排序
select *from item LIMIT 2 OFFSET 4;
	-- 7.查询价格小于2000的商品总数
select count(*) from item where item_price<2000; 
	-- 8.查询购物车中所有的数据
select * from cart;
	-- 9.查询每个用户加入购物车的商品总数
select user_id,sum(item_num) from cart GROUP BY user_id;
	-- 10.查询加入购物车的商品总数小于2 的用户
select user_id from cart GROUP BY user_id HAVING sum(item_num)<2; 
	-- 11.查询有加入购物车的商品
select DISTINCT item.*  from item INNER JOIN cart on item.item_id=cart.car_id;
	-- 12.查询没有加入购物车的商品
select * from item where item_id not in (SELECT item_id FROM cart);
-- 删除数据
	-- 1。用户4将商品1从购物车中移除
delete from cart where item_id=1 and user_id=4;
	-- 2.用户3 清空购物车
delete from cart where user_id=3;