create table category(
	category_id int(11) UNSIGNED not null auto_increment comment '分类id',
	category_name varchar(45) not null comment '分类名称',
	category_describe varchar(200) not null comment '分类描述',
	add_time int(11) not null comment '增加时间',
	update_time int(11) not null default 0 comment '更新时间',
	primary key (`category_id`)
)ENGINE=INNODB auto_increment=1 default charset=utf8 comment '分类表';

create table article(
	article_id int(11) UNSIGNED not null auto_increment comment '文章id',
	category_id int(11) not null comment '分类id',
	article_title varchar(45) not null comment '文章标题',
	article_intro varchar(200) not null comment '文章简介',
	article_content LONGTEXT not null comment '文章内容',
	add_time int(11) not null comment '发布时间',
	update_time int(11) not null DEFAULT 0 comment '更新时间',
	PRIMARY key(`article_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 comment='文章表';