use eshop;
-- 创建商品表item
create table item(
item_id int(11) UNSIGNED not null auto_increment,
item_name varchar(100) not null comment '商品名称',
item_price decimal(10,2) not null comment '商品价格',
primary key(`item_id`)	
) engine=innodb default charset=utf8mb4 comment='商品表';


-- 创建购物车表cart
Create table cart(
  `cart_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) unsigned NOT NULL COMMENT '用户id',
  `item_id` int(11) unsigned NOT NULL COMMENT '商品id',
  `item_num` int(11) unsigned NOT NULL DEFAULT '1' COMMENT '商品数量',
  `add_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '加入时间',
  PRIMARY KEY (`cart_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- 插入数据
-- 商品表
insert into item (item_name,item_price)
values
('小米全面屏电视 43英寸 E43K 全高清 DTS-HD 震撼音效 1GB+8GB 智能网络液晶平板电视 L43M5-EK',1349),
('TCL 55L8 55英寸 4K超高清电视 智慧语音 超薄机身 杜比+DTS双解码 网络教育 智能液晶平板电视机',2199),
('海信 VIDAA 43V1F-R 43英寸 全高清 海信电视 全面屏电视 智慧屏 1G+8G 教育电视 人工智能液晶平板电视	',1349)	,
('海信（Hisense）60E3F 60英寸 4K超高清 智慧语音 超薄悬浮全面屏大屏精致圆角液晶电视机 教育电视 人工智能',2699)	,
('TCL 50L8 50英寸 4K超高清电视 智慧语音 超薄机身 杜比+DTS双解码 网络教育 智能液晶平板电视机',1999),
('长虹 55D4P 55英寸超薄无边全面屏 4K超高清 手机投屏 智能网络 教育电视 平板液晶电视机（黑色）',2099);

-- 插入购物车表
insert into cart(user_id,item_id, item_num,add_time)
values
(1,1,1,UNIX_TIMESTAMP(NOW())),
(2,2,2,UNIX_TIMESTAMP(NOW())),
(3,4,1,UNIX_TIMESTAMP(NOW())),
(3,5,1,UNIX_TIMESTAMP(NOW())),
(4,1,2,UNIX_TIMESTAMP(NOW()));

-- ### 查询练习
-- 
-- 1. 查询商品表所有数据。
select * from item;
-- 查询购物车表数据
select * from cart;
-- 2. 查询价格小于2000的商品。
select * from item where item_price <2000;
-- 3. 查询名称中包含 海信 的商品。
select * from item where item_name like '%海信%';
-- 4. 查询商品表，假设每页3条记录，查询第1页的数据。
select * from item limit 3;
-- 5. 查询商品表，假设每页3条记录，查询第2页的数据。
select * from item limit 3 offset 2;
-- 6. 查询所有商品，按照价格进行升序排序。
select * from item order by item_price ASC;
--    查询所有商品，按照价格进行降序排序。
select * from item ORDER BY item_price DESC;
-- 7. 查询价格小于2000的商品总数。
select count(*) from item where item_price <2000;
-- 8. 查询购物车中所有数据。
select * from cart;
-- 9. 查询每个用户加入购物车的商品总数。
select user_id,sum(item_num)from cart group by user_id;
-- 10. 查询加入购物车的商品总数小于2的用户。
select user_id from cart group by user_id having sum(item_num)<2;
-- 11. 查询有加入购物车的商品。
-- 连接查询，DISTINCT表示去除重复的意思
select DISTINCT item.*from item inner join cart on item.item_id=cart.item_id;
-- 子查询方式，不推荐MySQL性能比较低
select * from item where item_id in(select item_id from cart);
-- 12. 查询没有加入购物车的商品。
select item.*from 
       item left join cart on item.item_id = cart.item_id
			 where isnull(cart.cart_id);
			 


			 
-- ### 修改数据
-- UPDATE <表名> SET 字段 1=值 1 [,字段 2=值 2… ] [WHERE 子句 ][ORDER BY 子句] [LIMIT 子句]	 	
			 
-- 示例1，修改商品表，商品id为1的价格为1249：
update item setitem_price = 1249 where item_id =1;

-- 示例2，用户3多加了一件商品5到购物车：
UPDATE cart set item_num=item_num+1 WHERE user_id=3 AND item_id=5;


-- ### 删除数据  DELETE FROM <表名> [WHERE 子句] [ORDER BY 子句] [LIMIT 子句]

-- 
-- 1. 用户4将将商品1从购物车中移除。
DELETE FROM cart WHERE user_id=4 AND item_id=1; 

-- 2. 用户3清空购物车。
delete from cart where user_id =3;

-- 3. 移除购物车中的所有数据：
truncate table cart;

-- 	1. 使用delete进行删除
-- 	2. 使用truncate进行删除
-- 	3. 对比delete和truncate的差别
-- 






